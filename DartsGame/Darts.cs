﻿using System;

namespace DartsGame
{
    public static class Darts
    {
        public static int GetScore(double x, double y)
        {
            double rad = (x * x) + (y * y);
            if (rad <= 1)
            {
                return 10;
            }
            else if (rad <= 25)
            {
                return 5;
            }
            else if (rad <= 100)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
